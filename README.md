# DRB Sentinel-1 Image AddOn

This addon enrich the `Sentinel-1 Product` topics with a preview image extractor.

## Available Images
|Product|Image name|Path|
|-------|----------|----|
|Level-1|preview|preview/quick-look.png|
|Level-1|{measurement}|measurement/{measurement}|
|Level-2|preview-rvl|preview/quick-look-l2-rvl.png|
|Level-2|preview-owi|preview/quick-look-l2-owi.png|
|Level-2|{measurement}|measurement/*.nc/root/variables/{measurement}|

## Example
```python
zip_node = resolver.create('/path/to/sentinel1.zip')
safe_node = zip_node[0]
# Retrieve the addon image object corresponding to the product (preview by default)
image = ImageAddon.apply(safe_node)
# The image name can be specified
image = ImageAddon.apply(safe_node, image_name='preview')
# Retrieve the drb-driver-image node corresponding to the addon using the default extraction
addon_image_node = image.image_node()
# Retrieve the rasterio implementation of the default image
dataset = image.get_impl(rasterio.DatasetReader) 
```