import re
import unittest

import drb.topics.resolver as resolver
import numpy as np
import rasterio
import xarray
from drb.drivers.image import DrbImageBaseNode
from drb.drivers.netcdf import DrbNetcdfVariableNode
from drb.image import ImageAddon


class TestSentinel1Image(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        cls.S1_level1 = "test/resources/S1_level1_test.zip"
        cls.S1_level2 = "test/resources/S1_level2_test.zip"
        cls.addon = ImageAddon()

    def test_level1_available_images(self):
        zip_node = resolver.create(self.S1_level1)
        safe_node = zip_node[0]
        images = self.addon.available_images(safe_node)
        images_names = [image[0] for image in images]
        self.assertIsNotNone(images)
        self.assertIsInstance(images, list)
        # 6 images in measurements folder + the preview one
        self.assertEqual(len(images), 7)
        self.assertTrue("preview" in images_names)
        self.assertTrue(
            (
                "s1a-iw1-slc-vh-20230710t055935"
                "-20230710t060000-049357-05ef66-001.tiff"
            )
            in images_names
        )

    def test_level2_available_images(self):
        zip_node = resolver.create(self.S1_level2)
        safe_node = zip_node[0]
        images = self.addon.available_images(safe_node)
        images_names = [image[0] for image in images]
        self.assertIsNotNone(images)
        self.assertIsInstance(images, list)
        # 110 images in netcfd + 2 previews
        self.assertEqual(len(images), 112)
        self.assertTrue("preview-rvl" in images_names)
        self.assertTrue("preview-owi" in images_names)
        self.assertTrue("owiMask" in images_names)

    def test_level1_preview_image(self):
        zip_node = resolver.create(self.S1_level1)
        safe_node = zip_node[0]
        image = self.addon.apply(safe_node)
        addon_image_node = image.image_node()

        image_node = safe_node["preview"]["quick-look.png"]

        data1 = addon_image_node.get_impl(rasterio.DatasetReader).read()
        data2 = image_node.get_impl(rasterio.DatasetReader).read()
        data3 = image.get_impl(rasterio.DatasetReader).read()

        self.assertEqual(image.name, "preview")
        self.assertIsInstance(addon_image_node, DrbImageBaseNode)
        self.assertEqual(addon_image_node.name, image_node.name)

        np.testing.assert_array_equal(data1, data2)
        np.testing.assert_array_equal(data2, data3)

    def test_level2_preview_image(self):
        zip_node = resolver.create(self.S1_level2)
        safe_node = zip_node[0]
        image = self.addon.apply(safe_node, image_name="preview-rvl")
        addon_image_node = image.image_node()

        image_node = safe_node["preview"]["quick-look-l2-rvl.png"]

        data1 = addon_image_node.get_impl(rasterio.DatasetReader).read()
        data2 = image_node.get_impl(rasterio.DatasetReader).read()
        data3 = image.get_impl(rasterio.DatasetReader).read()

        self.assertEqual(image.name, "preview-rvl")
        self.assertIsInstance(addon_image_node, DrbImageBaseNode)
        self.assertEqual(addon_image_node.name, image_node.name)

        np.testing.assert_array_equal(data1, data2)
        np.testing.assert_array_equal(data2, data3)
        zip_node = resolver.create(self.S1_level2)

        image = self.addon.apply(safe_node, image_name="preview-owi")
        addon_image_node = image.image_node()

        image_node = safe_node["preview"]["quick-look-l2-owi.png"]

        data1 = addon_image_node.get_impl(rasterio.DatasetReader).read()
        data2 = image_node.get_impl(rasterio.DatasetReader).read()
        data3 = image.get_impl(rasterio.DatasetReader).read()

        self.assertEqual(image.name, "preview-owi")
        self.assertIsInstance(addon_image_node, DrbImageBaseNode)
        self.assertEqual(addon_image_node.name, image_node.name)

        np.testing.assert_array_equal(data1, data2)
        np.testing.assert_array_equal(data2, data3)

    def test_level1_images(self):
        zip_node = resolver.create(self.S1_level1)
        safe_node = zip_node[0]
        image_name = (
            "s1a-iw1-slc-vh-20230710t055935"
            "-20230710t060000-049357-05ef66-001.tiff"
        )
        image = self.addon.apply(safe_node, image_name=image_name)
        addon_image_node = image.image_node()

        image_node = safe_node["measurement"][image_name]

        data1 = addon_image_node.get_impl(rasterio.DatasetReader).read()
        data2 = image_node.get_impl(rasterio.DatasetReader).read()
        data3 = image.get_impl(rasterio.DatasetReader).read()

        self.assertIsInstance(addon_image_node, DrbImageBaseNode)
        self.assertEqual(addon_image_node.name, image_node.name)

        np.testing.assert_array_equal(data1, data2)
        np.testing.assert_array_equal(data2, data3)

    def test_level2_images(self):
        zip_node = resolver.create(self.S1_level2)
        safe_node = zip_node[0]
        image = self.addon.apply(safe_node, image_name="owiMask")
        addon_image_node = image.image_node()

        image_node = safe_node["measurement"][0]["root"]["variables"][
            "owiMask"
        ]

        data1 = addon_image_node.get_impl(xarray.DataArray)
        data2 = image_node.get_impl(xarray.DataArray)
        data3 = image.get_impl(xarray.DataArray)

        self.assertIsInstance(addon_image_node, DrbNetcdfVariableNode)
        self.assertEqual(addon_image_node.name, image_node.name)

        self.assertTrue(data1.identical(data2))
        self.assertTrue(data2.identical(data3))
